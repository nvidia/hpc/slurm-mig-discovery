/**
 * Copyright (c) 2021, NVIDIA CORPORATION.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nvml.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

int devices[64];
int device_count = 0;

FILE *allowed_devices = NULL;
FILE *gres = NULL;

unsigned int get_minor_from_proc_cap_file(char *path)
{
    char read_buf[1024];
    FILE *file;
    
    if ((file = fopen(path, "r")) == NULL)
    {
        fprintf(stderr, "Could not open device file %s\n", path);
        exit(-1);
    }
    char *ptr;
    if (fgets(read_buf, sizeof(read_buf), file) == NULL)
    {
        fprintf(stderr, "Could not read device file %s\n", path);
        exit(-1);
    }
    if ((ptr = strchr(read_buf, ' ')) == NULL)
    {
        fprintf(stderr, "Error parsing device file\n");
        exit(-1);
    }
    return (atoi(ptr++));
}

void add_device(int id)
{
    devices[device_count++] = id;
    return;
}

int device_already_found(int id)
{
    int i;
    for (i = 0; i < device_count; i++)
    {
        if (devices[i] == id)
	{
            return 1;
        }
    }

    return 0;
}

int analyze_mig_device(int gpu_index, int mig_index, nvmlDevice_t device, unsigned int device_minor)
{
    nvmlDevice_t mig_device;
    nvmlDeviceAttributes_t attribs;
    char device_name[64];
    char g_cap_proc_path[1024];
    char c_cap_proc_path[1024];

    char g_cap_dev_path[1024];
    char c_cap_dev_path[1024];

    unsigned int g_slice_count;
    unsigned int c_slice_count;
    unsigned int g_id;
    unsigned int c_id;
    unsigned int memoryGB;
    unsigned int c_device_cap;
    unsigned int g_device_cap;

    if (nvmlDeviceGetMigDeviceHandleByIndex(device, mig_index, &mig_device) != NVML_SUCCESS)
    {
        /* no more devices */
        return 0;
    }
    if (nvmlDeviceGetAttributes_v2(mig_device, &attribs) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetAttributes()\n");
        exit(-1);	
    }
    if (nvmlDeviceGetGpuInstanceId(mig_device, &g_id) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetGpuInstanceId()\n");
        exit(-1);	
    }
    if (nvmlDeviceGetComputeInstanceId(mig_device, &c_id) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetComputeInstanceId()\n");
        exit(-1);	
    }
    
    memoryGB = (attribs.memorySizeMB + 1024 - 1) / 1024;
    
    g_slice_count = attribs.gpuInstanceSliceCount;
    c_slice_count = attribs.computeInstanceSliceCount;
    
    if (c_slice_count != g_slice_count)
    {
        snprintf(device_name, sizeof(device_name), "%dc.%dg.%dgb",
            c_slice_count,
            g_slice_count,
            memoryGB);
    } 
    else
    {
        snprintf(device_name, sizeof(device_name), "%dg.%dgb",
            g_slice_count,
            memoryGB);
    }
    
    snprintf(g_cap_proc_path, sizeof(g_cap_proc_path),
    		"/proc/driver/nvidia/capabilities/gpu%d/mig/gi%d/access",
    		device_minor,
    		g_id);
    snprintf(c_cap_proc_path, sizeof(c_cap_proc_path),
    		"/proc/driver/nvidia/capabilities/gpu%d/mig/gi%d/ci%d/access",
    		device_minor,
    		g_id,
    		c_id);
    
    c_device_cap = get_minor_from_proc_cap_file(c_cap_proc_path);
    g_device_cap = get_minor_from_proc_cap_file(g_cap_proc_path);
    
    if (!device_already_found(g_device_cap))
    {
        snprintf(g_cap_dev_path, sizeof(g_cap_dev_path),
    	    "/dev/nvidia-caps/nvidia-cap%d",
    	    g_device_cap);
    	
        snprintf(c_cap_dev_path, sizeof(c_cap_dev_path),
            "/dev/nvidia-caps/nvidia-cap%d",
            c_device_cap);
    	
        fprintf(gres,"# GPU %d MIG %d %s\n", gpu_index, mig_index, g_cap_proc_path);
        fprintf(gres,"Name=gpu Type=%s File=%s\n\n", device_name, g_cap_dev_path);
    	
        add_device(g_device_cap);
    }
    
    if (!device_already_found(c_device_cap))
    {
        snprintf(c_cap_dev_path, sizeof(c_cap_dev_path),
            "/dev/nvidia-caps/nvidia-cap%d", c_device_cap);
    
        fprintf(allowed_devices, "# GPU %d MIG %s\n", gpu_index, c_cap_proc_path);
        fprintf(allowed_devices, "%s\n\n", c_cap_dev_path);
        add_device(c_device_cap);
    }
    
    return 0;
}

int analyze_gpu(int gpu_index) {
    nvmlDevice_t device;
    nvmlDevice_t mig_device;
    char name[32];
    int max_mig;
    int device_minor;
    int i;
    
    if (nvmlDeviceGetHandleByIndex(gpu_index, &device) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetHandleByIndex()\n");
        exit(-1);	
    }
    if (nvmlDeviceGetName(device, name, sizeof(name)) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetName()\n");
        exit(-1);	
    }
    if (nvmlDeviceGetMinorNumber(device, &device_minor) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetMinorNumber()\n");
        exit(-1);	
    }
    if (nvmlDeviceGetMaxMigDeviceCount(device, &max_mig) != NVML_SUCCESS)
    {
        fprintf(stderr, "Error in nvmlDeviceGetMaxMigDeviceCount()\n");
        exit(-1);	
    }
    
    if (max_mig == 0)
    {
        fprintf(gres, "# GPU %d\n", gpu_index);
        fprintf(gres, "Name=gpu Type=ampere File=/dev/nvidia%d\n\n", device_minor);
        return 0;
    }
    
    if (nvmlDeviceGetMigDeviceHandleByIndex(device, 0, &mig_device) != NVML_SUCCESS)
    {
    	fprintf(stderr, "GPU %d MIG enabled but no MIG devices, skipping\n", device_minor);
	return 0;
    }

    for (i = 0; i < max_mig; i++)
    {
        if (!device_already_found(device_minor))
	{
            fprintf(allowed_devices, "# GPU %d\n", device_minor);
            fprintf(allowed_devices, "/dev/nvidia%d\n\n", device_minor);
    	    add_device(gpu_index);
        }
    
        analyze_mig_device(gpu_index, i, device, device_minor);
    }
    return 0;
}

int main(int argc, char *argv)
{
    int i;
    unsigned int device_count;
    nvmlReturn_t ret;
    nvmlInit_v2();
    
    memset(devices, 0, sizeof(devices));
    
    if ((gres = fopen("gres.conf", "w")) == NULL)
    {
        fprintf(stderr, "Could not open gres.conf for writing\n");
        exit(-1);
    }
    
    if ((allowed_devices = fopen("cgroup_allowed_devices_file.conf", "w")) == NULL)
    {
        fprintf(stderr, "Could not open cgroup_allowed_devices_file.conf for writing\n");
        exit(-1);
    }
    
    ret = nvmlDeviceGetCount(&device_count);
    
    fprintf(stdout,"GPU count %d\n", device_count);
    
    for (i = 0; i < device_count; i++)
    {
        analyze_gpu(i);
    }
    
    fprintf(allowed_devices, "/dev/null\n");
    fprintf(allowed_devices, "/dev/urandom\n");
    fprintf(allowed_devices, "/dev/zero\n");
    fprintf(allowed_devices, "/dev/cpu/*/*\n");
    fprintf(allowed_devices, "/dev/pts/*\n");
    
    fclose(gres);
    fclose(allowed_devices);

    fprintf(stdout, "Success\n");
    return 0;
}
